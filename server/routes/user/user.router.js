const express = require('express');
const User = require('../../db/models/user.model');

const userRouter = express.Router();

userRouter.post('/', (req, res) => {
    const {email, password} = req.body;
    
    const user = new User(
        {
            email,
            password
        }
    );

    user.save().then(
        (doc) => {
            res.json(doc);
        },
        (err) => {
            res.status(400).json(err);
        }
    );
});

module.exports = userRouter;