const express = require('express');
const userRouter = require('../user/user.router');

const apiRouter = express.Router();

module.exports = apiRouter;