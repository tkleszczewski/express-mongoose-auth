const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const PORT = process.env.PORT || 3000;

const apiRouter = require('./routes/api/api.router');
const userRouter = require('./routes/user/user.router');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use('/api', apiRouter);
app.use('/users', userRouter);

app.listen(PORT, () => {
    console.log(`Server running on port: ${PORT}`);
});