const mongoose = require('../mongoose');
const validator = require('validator');

const Schema = mongoose.Schema;

const User = new Schema({
    email: {
        type: String,
        required: [true, 'email is required'],
        trim: true,
        minlength: 1,
        unique: [true, '{VALUE} is already used by another user'],
        validate: {
            validator: validator.isEmail,
            message: '{VALUE} is not a valid email'
        }
    },
    password: {
        type: String,
        required: [true, 'password is required'],
        minlength: 8
    },
    tokens: [{
        access: {
            type: String,
            required: true
        },
        token: {
            type: String,
            required: true
        }
    }]
})

module.exports = mongoose.model('User', User);